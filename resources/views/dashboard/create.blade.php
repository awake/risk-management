@extends('layouts.dashboard')
@section('sidebar')
	<ul>
		<li><a href="">Add User</a></li>
		<li><a href="">Add Role</a></li>
	</ul>
@endsection
@section('content')
	<div class="form-title">
		<h2 class="title">Create a new User</h2>
		<a href=""><button type="button" class="gray">Cancel</button></a>
	</div>

	<form method="POST" action="">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="text" name="username" placeholder = "Payroll Number">
		<input type="email" name="email" placeholder = "Email">
		<input type="text" name="firstname" placeholder = "First Name">
		<input type="text" name="lastname" placeholder = "Last Name">
		<input type="password" name="password" placeholder = "Password">
		<input type="password" name="confirm_password" placeholder = "Confirm Password">
		
		<button type="submit" class="submit">Create User</button>
	</form>
@stop	