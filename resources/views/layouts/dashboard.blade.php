<html>
<head>
	<title>Risk Management - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<nav id="navbar">
		<h1 class="logo-icon"><a href=""><i class="fa fa-th" aria-hidden="true"></i></a></h1>
		<ul class="tabs">
			<li class="active"><a href=""><i class="fa fa-file-text"></i></a></li>
			<li><a href=""><i class="fa fa-comment"></i></a></li>
			<li><a href=""><img src="{{asset('img/man.jpg')}}"></a></li>
		</ul>
	</nav>

	<div class="wrapper">
		<!-- Sidebar -->
		<div id="sidebar">
			@yield('sidebar')
		</div>
		<!-- Content -->
		<div class="content">
			@yield('content')
		</div>
	</div>
</body>
</html>