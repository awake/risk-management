<html>
<head>
	<title>Risk Management - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body>
	<nav id="navbar">
		<h1 class="logo"><img src="{{asset('img/logo.png')}}"></h1>
	</nav>
	<div class="banner"></div>
	<div class="slide"></div>
	<div class="login-form" id="loginForm">
		<h1>Welcome to TIME!</h1>
		<p>Please login using your personal account details.</p>
		<form method="POST" action="{{ url('/auth/login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="email" placeholder="Payroll number" name="email" required>
			<input type="password" placeholder="Password" name="password" required>
			<div class="buttons">
				<button type="submit" class="dark">Sign In</button>
				&nbsp;
				<a href="{{ url('/password/email') }}"><button type="button" class="light">Forgot Password</button></a>
			</div>
		</form>
	</div>
</body>
</html>